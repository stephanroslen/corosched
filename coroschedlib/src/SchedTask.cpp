// 2022 by Stephan Roslen

#include "SchedTask.h"

#include "QueueRunner.h"

namespace corosched::detail {

std::coroutine_handle<> handleCont(std::coroutine_handle<> cont, QueueRunnerPromiseType& qrp) noexcept
{
    qrp.enqueueCoroutine(cont);
    return qrp.nextCoroutine();
}

void enqueueNewTask(SchedTask<void>&& newTask, QueueRunnerPromiseType& qrp) noexcept
{
    qrp.enqueueTask(std::move(newTask));
}

}