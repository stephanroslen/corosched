// 2022 by Stephan Roslen

#pragma once

#include "ExceptionPromiseBase.h"
#include "SchedTask.h"

#include <fmt/core.h>

#include <cassert>
#include <coroutine>
#include <exception>
#include <queue>

namespace corosched::detail {

struct QueueRunner;

struct QueueRunnerPromiseType : ExceptionPromiseBase {
    using promise_type = QueueRunnerPromiseType;

    struct DoRun {
    };

    using CoroutineQueue = std::queue<std::coroutine_handle<>>;
    using TaskType = SchedTask<void>;
    using TaskQueue = std::queue<TaskType>;

    std::exception_ptr exception{};

    TaskQueue taskQueue{};
    CoroutineQueue coroutineQueue{};

    QueueRunnerPromiseType() = delete;

    using Handle = std::coroutine_handle<promise_type>;

    void enqueueCoroutine(std::coroutine_handle<> cr) noexcept
    {
        coroutineQueue.emplace(cr);
    }

    void enqueueTask(TaskType&& task) noexcept
    {
        taskQueue.emplace(std::move(task));
    }

    std::coroutine_handle<> nextCoroutine() noexcept
    {
        if (!taskQueue.empty()) {
            auto task = std::move(taskQueue.front());
            taskQueue.pop();
            return task.workHandleSelfDestruct(std::coroutine_handle<QueueRunnerPromiseType>::from_promise(*this), this);
        }
        if (!coroutineQueue.empty()) {
            auto c{coroutineQueue.front()};
            coroutineQueue.pop();
            return c;
        }
        return {};
    }

    auto await_transform(DoRun)
    {
        struct Awaiter {
            promise_type& p;
            bool continued{false};

            constexpr bool await_ready() const noexcept
            {
                return false;
            }

            std::coroutine_handle<> await_suspend(std::coroutine_handle<>) noexcept
            {
                auto next{p.nextCoroutine()};
                if (next) {
                    continued = true;
                    return next;
                }
                return std::coroutine_handle<promise_type>::from_promise(p);
            }

            bool await_resume() const noexcept
            {
                return continued;
            }
        };
        return Awaiter{*this};
    }

    constexpr std::suspend_never initial_suspend() const noexcept
    {
        return {};
    }

    constexpr std::suspend_always final_suspend() const noexcept
    {
        return {};
    }

    constexpr void return_void() const noexcept
    {}

    QueueRunner get_return_object() noexcept;

private:
    QueueRunnerPromiseType(TaskQueue&& tq) noexcept
        : taskQueue{std::move(tq)}
    {}

    friend QueueRunner runQueueRunnerHelper(TaskQueue&&);
};

struct QueueRunner {
    using promise_type = QueueRunnerPromiseType;
    using DoRun = typename promise_type::DoRun;
    using Handle = typename promise_type::Handle;
    using TaskQueue = typename promise_type::TaskQueue;

    static constexpr DoRun doRun{};

    constexpr QueueRunner(QueueRunner&& other) noexcept
        : mHandle{std::exchange(other.mHandle, nullptr)} {};

    constexpr QueueRunner& operator=(QueueRunner&& other) noexcept
    {
        if (std::addressof(other) == this) {
            return *this;
        }
        if (mHandle) {
            mHandle.destroy();
        }
        mHandle = std::exchange(other.mHandle, nullptr);
        return *this;
    }

    constexpr ~QueueRunner()
    {
        if (mHandle) {
            mHandle.destroy();
        }
    }

private:
    Handle mHandle{};

    promise_type& promise() const noexcept
    {
        return mHandle.promise();
    }

    constexpr QueueRunner(Handle handle) noexcept
        : mHandle{handle} {};

    friend QueueRunnerPromiseType;
};

inline QueueRunner QueueRunnerPromiseType::get_return_object() noexcept
{
    return QueueRunner{std::coroutine_handle<promise_type>::from_promise(*this)};
}

inline QueueRunner runQueueRunnerHelper(QueueRunner::TaskQueue&&)
{
    while (co_await QueueRunner::doRun) {
    };
}

inline void runQueueRunner(QueueRunner::TaskQueue&& taskQueue)
{
    runQueueRunnerHelper(std::move(taskQueue));
}

}