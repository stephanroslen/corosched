// 2022 by Stephan Roslen

#pragma once

#include <exception>

namespace corosched::detail {

struct ExceptionPromiseBase {
    std::exception_ptr exception;

    void unhandled_exception() noexcept
    {
        exception = std::current_exception();
    }

    void rethrowIfException() const
    {
        if (exception) {
            std::rethrow_exception(exception);
        }
    }

protected:
    ~ExceptionPromiseBase() = default;
};

}