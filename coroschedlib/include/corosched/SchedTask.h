// 2022 by Stephan Roslen

#pragma once

#include "ExceptionPromiseBase.h"
#include "StoringPromiseBase.h"

#include <cassert>
#include <coroutine>
#include <exception>
#include <iostream>
#include <variant>

namespace corosched {

template <typename T>
struct SchedTask;

namespace detail {

struct QueueRunnerPromiseType;

struct DoYield {
};

struct NewTask {
    SchedTask<void>&& task;
};

std::coroutine_handle<> handleCont(std::coroutine_handle<> cont, QueueRunnerPromiseType& qrp) noexcept;
void enqueueNewTask(SchedTask<void>&& newTask, QueueRunnerPromiseType& qrp) noexcept;

}

constexpr detail::DoYield doYield{};

constexpr detail::NewTask newTask(SchedTask<void>&& task)
{
    return {std::move(task)};
}

template <typename T>
struct SchedTask {
    struct promise_type : detail::ExceptionPromiseBase, detail::StoringPromiseBase<T> {
        bool selfDestruct{false};
        std::coroutine_handle<> continuation{};
        detail::QueueRunnerPromiseType* qrp{nullptr};

        using Handle = std::coroutine_handle<promise_type>;

        constexpr std::suspend_always initial_suspend() const noexcept
        {
            return {};
        }

        auto await_transform(detail::DoYield) const noexcept
        {
            struct Awaiter {
                detail::QueueRunnerPromiseType& qrp;

                constexpr bool await_ready() const noexcept
                {
                    return false;
                }

                std::coroutine_handle<> await_suspend(std::coroutine_handle<> in) noexcept
                {
                    return handleCont(in, qrp);
                }

                constexpr void await_resume() const noexcept {};
            };
            return Awaiter{*qrp};
        }

        auto await_transform(detail::NewTask&& task) const noexcept
        {
            enqueueNewTask(std::move(task.task), *qrp);
            return std::suspend_never{};
        }

        template <typename T_>
        auto await_transform(SchedTask<T_>&& schedTask) const noexcept
        {
            struct Awaiter {
                SchedTask<T_>&& schedTask;
                detail::QueueRunnerPromiseType* qrp;

                constexpr bool await_ready() const noexcept
                {
                    return false;
                }

                constexpr std::coroutine_handle<> await_suspend(std::coroutine_handle<> cont) const noexcept
                {
                    auto& promise{schedTask.mHandle.promise()};
                    promise.continuation = cont;
                    promise.qrp = qrp;
                    return schedTask.mHandle;
                }

                constexpr T_ await_resume() const
                {
                    const auto& promise{schedTask.mHandle.promise()};
                    promise.rethrowIfException();
                    assert(promise.valueSetIfNonVoid());
                    return promise.getRetVal();
                }
            };
            return Awaiter{std::move(schedTask), qrp};
        }

        constexpr auto final_suspend() noexcept
        {
            struct Awaiter {
                promise_type& p;

                constexpr bool await_ready() const noexcept
                {
                    return false;
                }

                auto await_suspend(std::coroutine_handle<>) const noexcept
                {
                    const auto cont{p.continuation};
                    if (p.selfDestruct) {
                        std::coroutine_handle<promise_type>::from_promise(p).destroy();
                    }
                    return cont;
                }

                constexpr void await_resume() const noexcept {};
            };
            return Awaiter{*this};
        }

        SchedTask get_return_object() noexcept
        {
            return SchedTask{Handle::from_promise(*this)};
        }
    };

    using Handle = typename promise_type::Handle;

    constexpr ~SchedTask()
    {
        if (mHandle) {
            mHandle.destroy();
        }
    }

    constexpr SchedTask(SchedTask&& other) noexcept
        : mHandle{std::exchange(other.mHandle, nullptr)} {};
    constexpr SchedTask& operator=(SchedTask&& other) noexcept
    {
        if (std::addressof(other) == this) {
            return *this;
        }
        if (mHandle) {
            mHandle.destroy();
        }
        mHandle = std::exchange(other.mHandle, nullptr);
        return *this;
    }

    Handle workHandleSelfDestruct(std::coroutine_handle<> cont, detail::QueueRunnerPromiseType* qrp) noexcept
    {
        promise().selfDestruct = true;
        promise().continuation = cont;
        promise().qrp = qrp;
        return std::exchange(mHandle, nullptr);
    }

private:
    Handle mHandle{};

    promise_type& promise() const noexcept
    {
        return mHandle.promise();
    }

    constexpr SchedTask(Handle handle) noexcept
        : mHandle{handle}
    {}

    template <typename T_>
    friend struct SchedTask;
};

}