// 2022 by Stephan Roslen

#pragma once

#include <optional>

namespace corosched::detail {

template <typename T>
struct StoringPromiseBase {
    std::optional<T> retVal{};

    constexpr bool valueSetIfNonVoid() const noexcept
    {
        return retVal.has_value();
    }

    constexpr const T& getRetVal() const& noexcept
    {
        return *retVal;
    }

    constexpr T&& getRetVal() && noexcept
    {
        return *std::move(retVal);
    }

    constexpr void return_value(const T& val) noexcept
    {
        retVal = val;
    }

protected:
    ~StoringPromiseBase() = default;
};

template <>
struct StoringPromiseBase<void> {
    constexpr void return_void() const noexcept
    {}

    constexpr bool valueSetIfNonVoid() const noexcept
    {
        return true;
    }

    constexpr void getRetVal() const noexcept {};

protected:
    ~StoringPromiseBase() = default;
};

}