// 2022 by Stephan Roslen

#pragma once

#include "QueueRunner.h"
#include "SchedTask.h"

#include <cassert>
#include <coroutine>
#include <exception>

namespace corosched {

struct QueueScheduler {
    using QueueType = detail::QueueRunner::TaskQueue;

    void enqueue(SchedTask<void>&& task)
    {
        mQueue.emplace(std::move(task));
    }

    void start()
    {
        detail::runQueueRunner(std::move(mQueue));
    }

private:
    QueueType mQueue{};
};

}