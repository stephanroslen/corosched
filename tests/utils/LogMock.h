// 2022 by Stephan Roslen

#pragma once

#include <gmock/gmock.h>

class LogMock {
public:
    MOCK_METHOD(void, logSzT, (const size_t), (const));
    MOCK_METHOD(void, logStringView, (const std::string_view), (const));
};