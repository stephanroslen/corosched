// 2022 by Stephan Roslen

#include "utils/LogMock.h"

#include <corosched/QueueScheduler.h>
#include <corosched/SchedTask.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string_view>

#include <fmt/core.h>

using namespace corosched;

namespace {

SchedTask<void> newTaskTest(size_t cnt, size_t rValue, bool yield, const LogMock& logMock)
{
    logMock.logSzT(cnt);
    if (cnt > 0) {
        for (size_t i{0}; i < rValue; ++i) {
            co_await newTask(newTaskTest(cnt - 1, rValue, yield, logMock));
        }
    }
    if (yield) {
        co_await doYield;
    }
    logMock.logSzT(cnt);
    co_return;
}

}

TEST(TestNewTask, Growth)
{
    ::testing::InSequence s;

    const LogMock logMock{};

    QueueScheduler qs{};

    qs.enqueue(newTaskTest(3, 2, false, logMock));

    EXPECT_CALL(logMock, logSzT(3)).Times(2);
    EXPECT_CALL(logMock, logSzT(2)).Times(4);
    EXPECT_CALL(logMock, logSzT(1)).Times(8);
    EXPECT_CALL(logMock, logSzT(0)).Times(16);

    qs.start();
}

TEST(TestNewTask, Yielding)
{
    ::testing::InSequence s;

    const LogMock logMock{};

    QueueScheduler qs{};

    qs.enqueue(newTaskTest(3, 1, true, logMock));

    EXPECT_CALL(logMock, logSzT(3)).Times(1);
    EXPECT_CALL(logMock, logSzT(2)).Times(1);
    EXPECT_CALL(logMock, logSzT(1)).Times(1);
    EXPECT_CALL(logMock, logSzT(0)).Times(1);
    EXPECT_CALL(logMock, logSzT(3)).Times(1);
    EXPECT_CALL(logMock, logSzT(2)).Times(1);
    EXPECT_CALL(logMock, logSzT(1)).Times(1);
    EXPECT_CALL(logMock, logSzT(0)).Times(1);

    qs.start();
}