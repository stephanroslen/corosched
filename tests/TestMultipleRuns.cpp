// 2022 by Stephan Roslen

#include "utils/LogMock.h"

#include <corosched/QueueScheduler.h>
#include <corosched/SchedTask.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>
#include <string_view>

using namespace corosched;

namespace {

SchedTask<std::string> genString(size_t arg, const LogMock& logMock)
{
    logMock.logSzT(arg);
    const std::string result(arg, '*');
    logMock.logStringView(result);
    co_return result;
}

SchedTask<void> qsTest(size_t arg, const LogMock& logMock)
{
    logMock.logSzT(arg);
    const auto tmp{co_await genString(arg, logMock)};
    logMock.logStringView(tmp);
}

}

TEST(TestMultipleRuns, Simple)
{
    ::testing::InSequence s;

    const LogMock logMock{};

    QueueScheduler qs{};

    for (int i{0}; i < 3; ++i) {
        qs.enqueue(qsTest(i, logMock));
    }

    EXPECT_CALL(logMock, logSzT(0)).Times(2);
    EXPECT_CALL(logMock, logStringView("")).Times(2);

    EXPECT_CALL(logMock, logSzT(1)).Times(2);
    EXPECT_CALL(logMock, logStringView("*")).Times(2);

    EXPECT_CALL(logMock, logSzT(2)).Times(2);
    EXPECT_CALL(logMock, logStringView("**")).Times(2);

    qs.start();
}

TEST(TestMultipleRuns, TwoStages)
{
    ::testing::InSequence s;

    const LogMock logMock{};

    QueueScheduler qs{};

    for (int i{0}; i < 3; ++i) {
        qs.enqueue(qsTest(i, logMock));
    }

    EXPECT_CALL(logMock, logSzT(0)).Times(2);
    EXPECT_CALL(logMock, logStringView("")).Times(2);

    EXPECT_CALL(logMock, logSzT(1)).Times(2);
    EXPECT_CALL(logMock, logStringView("*")).Times(2);

    EXPECT_CALL(logMock, logSzT(2)).Times(2);
    EXPECT_CALL(logMock, logStringView("**")).Times(2);

    qs.start();

    for (int i{5}; i < 8; ++i) {
        qs.enqueue(qsTest(i, logMock));
    }

    EXPECT_CALL(logMock, logSzT(5)).Times(2);
    EXPECT_CALL(logMock, logStringView("*****")).Times(2);

    EXPECT_CALL(logMock, logSzT(6)).Times(2);
    EXPECT_CALL(logMock, logStringView("******")).Times(2);

    EXPECT_CALL(logMock, logSzT(7)).Times(2);
    EXPECT_CALL(logMock, logStringView("*******")).Times(2);

    qs.start();
}