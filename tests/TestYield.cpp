// 2022 by Stephan Roslen

#include "utils/LogMock.h"

#include <corosched/QueueScheduler.h>
#include <corosched/SchedTask.h>
#include <fmt/format.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string_view>

using namespace corosched;

namespace {

SchedTask<void> ppTest(size_t cnt, std::string_view sv, const LogMock& logMock)
{
    for (size_t i{0}; i < cnt; ++i) {
        logMock.logSzT(i);
        const std::string output{fmt::format("{}: {}", i, sv)};
        logMock.logStringView(output);
        co_await doYield;
    }
    co_return;
}

}

TEST(TestYield, Balanced)
{
    ::testing::InSequence s;

    const LogMock logMock{};

    QueueScheduler qs{};

    qs.enqueue(ppTest(3, "Ping", logMock));
    qs.enqueue(ppTest(3, "Pong", logMock));

    EXPECT_CALL(logMock, logSzT(0)).Times(1);
    EXPECT_CALL(logMock, logStringView("0: Ping"));
    EXPECT_CALL(logMock, logSzT(0)).Times(1);
    EXPECT_CALL(logMock, logStringView("0: Pong"));

    EXPECT_CALL(logMock, logSzT(1)).Times(1);
    EXPECT_CALL(logMock, logStringView("1: Ping"));
    EXPECT_CALL(logMock, logSzT(1)).Times(1);
    EXPECT_CALL(logMock, logStringView("1: Pong"));

    EXPECT_CALL(logMock, logSzT(2)).Times(1);
    EXPECT_CALL(logMock, logStringView("2: Ping"));
    EXPECT_CALL(logMock, logSzT(2)).Times(1);
    EXPECT_CALL(logMock, logStringView("2: Pong"));

    qs.start();
}

TEST(TestYield, MoreFirst)
{
    ::testing::InSequence s;

    const LogMock logMock{};

    QueueScheduler qs{};

    qs.enqueue(ppTest(3, "Ping", logMock));
    qs.enqueue(ppTest(1, "Pong", logMock));

    EXPECT_CALL(logMock, logSzT(0)).Times(1);
    EXPECT_CALL(logMock, logStringView("0: Ping"));
    EXPECT_CALL(logMock, logSzT(0)).Times(1);
    EXPECT_CALL(logMock, logStringView("0: Pong"));

    EXPECT_CALL(logMock, logSzT(1));
    EXPECT_CALL(logMock, logStringView("1: Ping"));

    EXPECT_CALL(logMock, logSzT(2));
    EXPECT_CALL(logMock, logStringView("2: Ping"));

    qs.start();
}

TEST(TestYield, MoreSecond)
{
    ::testing::InSequence s;

    const LogMock logMock{};

    QueueScheduler qs{};

    qs.enqueue(ppTest(1, "Ping", logMock));
    qs.enqueue(ppTest(3, "Pong", logMock));

    EXPECT_CALL(logMock, logSzT(0)).Times(1);
    EXPECT_CALL(logMock, logStringView("0: Ping"));
    EXPECT_CALL(logMock, logSzT(0)).Times(1);
    EXPECT_CALL(logMock, logStringView("0: Pong"));

    EXPECT_CALL(logMock, logSzT(1));
    EXPECT_CALL(logMock, logStringView("1: Pong"));

    EXPECT_CALL(logMock, logSzT(2));
    EXPECT_CALL(logMock, logStringView("2: Pong"));

    qs.start();
}